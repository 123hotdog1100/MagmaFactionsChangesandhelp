++++++++
Commands
++++++++

General
=======
* ``/spawn`` - Teleports you to the spawn point
* ``/sethome`` - Sets your home location
* ``/pvp`` - Toggle PVP on/off
* ``/ignore &a<player>`` - Show the ignore list or ignore a player
* ``/money`` - Shows your money
* ``/pay [player] [amount]`` - Send money to a player
* ``/money top`` - This displays the top 5 richest players
* ``/suicide`` - Kills yourself
* ``/stats`` - Shows your ontime
* ``/vote`` - Vote for our server and get rewards
* ``/ts`` - Show our TS3 IP
* ``/website`` - Show our website url
* ``/forum`` - Show our forum url
* ``/buy`` - Show our in game shop menu for donations

Chat
====
* ``/msg [player] [message]`` - Sends a private message
* ``/r [message]`` - Reply to the player
* ``/mail send [player] [message]`` -  Send a offline message
* ``/mail read`` - Check your offline messages
* ``/mail clear`` - Remove your messages

Claims
======

See :ref:`ref-griefprevention` for more information.

Shop
======
* ``/shop`` - Opens in game shop menu

Tickets
=======
* 'to create a ticket please goto the forum to seek help'

Donator
=======
*========[DONATOR Rank - �1 GBP]========

*--------------=[ FEATURES ]=--------------
*- The Vip rank and tag
*   - Vip kit every 48 hours
*- You can set 3 homes

*--------------=[ COMMANDS ]=--------------
*- /eat

*- /near

*- /craft

*--------------=[ KITS ]=--------------
*You can use /kit donator every 48 hours, check the kit preveiws at /warp ranks

*========[ Vip - $4 USD ]========


*--------------=[ FEATURES ]=--------------
*- Vip Rank and Colored name
*- Can set 5 homes

*--------------=[ COMMANDS ]=--------------

*- /back

*- /near

*- /craft
*- /eat-
*- /echest

*--------------=[ KITS ]=--------------
*You can use /kit Vip Every 48 Hours

*========[ Champion - �5 GBP ]=======

*--------------=[ FEATURES ]=--------------
*- Elite Rank and Colored name

*- Can set 6 homes


*--------------=[ COMMANDS ]=--------------

*- /back


*- /near

*- /craft
*- /eat
*- /echest

*--------------=[ KITS ]=--------------
*You can use /kit Elite Every 48 Hours

 
*========[ HERO - �15 GBP ]=======

*--------------=[ FEATURES ]=--------------
*- HERO Rank and Colored name

*- Can set 12 homes


*--------------=[ COMMANDS ]=--------------

*- /back

*- /fly

*- /near

*- /fixall

*- /craft
*- /eat-
*- /echest

*--------------=[ KITS ]=--------------
*You can use /kit HERO Every 48 Hours
